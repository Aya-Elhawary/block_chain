# Importing the libraries
import datetime
import hashlib
import json
from flask import Flask, jsonify, request
import requests
from uuid import uuid4
from urllib.parse import urlparse


#  Building a Blockchain

class Blockchain:

    def __init__(self):
        self.chain = []
        self.transactions = []
        self.nodes = set()
        self.create_block(proof=1, previous_hash='0')

    def create_block(self, proof, previous_hash):
        block = {'index': len(self.chain) + 1,
                 'timestamp': str(datetime.datetime.now()),
                 'proof': proof,
                 'previous_hash': previous_hash,
                 'transactions': self.transactions
                 }
        self.transactions=[]
        self.chain.append(block)

        return block

    def get_previous_block(self):
        return self.chain[-1]

    def proof_of_work(self, previous_proof):
        new_proof = 1
        check_proof = False
        while check_proof is False:
            hash_operation = hashlib.sha256(str(new_proof ** 2 - previous_proof ** 2).encode()).hexdigest()
            if hash_operation[:4] == '0000':
                check_proof = True
            else:
                new_proof += 1
        return new_proof

    def hash(self, block):
        encoded_block = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha256(encoded_block).hexdigest()

    def is_chain_valid(self, chain):
        previous_block = chain[0]
        block_index = 1
        while block_index < len(chain):
            block = chain[block_index]
            if block['previous_hash'] != self.hash(previous_block):
                return False
            previous_proof = previous_block['proof']
            proof = block['proof']
            hash_operation = hashlib.sha256(str(proof ** 2 - previous_proof ** 2).encode()).hexdigest()
            if hash_operation[:4] != '0000':
                return False
            previous_block = block
            block_index += 1
        return True

    def add_transaction(self, sender, reciever, amount):
        self.transactions.append({'sender':sender,
                           'reciever':reciever,
                           'amount' :amount})
        pervious_block=self.get_previous_block()
        return pervious_block['index']+1

    ###AddNode
    def add_node(self,adress):
        url_parsed= urlparse(adress)
        self.nodes.add(url_parsed.netloc)
##########################check if the original chain is replaced
    def replace_chain(self):
        Network=self.nodes
        longest_chain=None
        maxLength=len(self.chain)
        for nodes in Network:
            response = requests.get(f'http://{nodes}/get_chain')
            if response.status_code == 200:
                lenght=response.json('length')
                chain =response.json('chain')
                if lenght>maxLength and self.is_chain_valid(chain):
                    maxLength=lenght
                    longest_chain=chain
        if longest_chain :
            self.chain=longest_chain
            return True
        return False

app = Flask(__name__)

#

# Part 2 - Mining our Blockchain

# Creating Web App
app = Flask(__name__)
node=str(uuid4()).replace('-','')
# Creating a Blockchain object
blockchain = Blockchain()


# Mining a new block
@app.route('/mine_block', methods=['GET'])
def mine_block():
    previous_block = blockchain.get_previous_block()
    previous_proof = previous_block['proof']
    proof = blockchain.proof_of_work(previous_proof)
    previous_hash = blockchain.hash(previous_block)
    block = blockchain.create_block(proof, previous_hash)
    blockchain.add_transaction(node,'Aya',amount=11211222)
    blockchain.add_transaction(node, 'Mostafa', amount=11211222)#????????????
    response = {'message': 'Congratulations, you just mined a block!',
                'index': block['index'],
                'timestamp': block['timestamp'],
                'proof': block['proof'],
                'previous_hash': block['previous_hash'],
                 'transactions' : block['transactions']}
    return jsonify(response), 200


# Getting the full Blockchain
@app.route('/get_chain', methods=['GET'])
def get_chain():
    response = {'chain': blockchain.chain,
                'length': len(blockchain.chain)}
    return jsonify(response), 200

######add transaction
@app.route('/add_transaction', methods=['POST'])
def add_transaction():
    json=request.get_json()
    transaction_keys=['sender','reciever','amount']
    if not all(key in json for key in transaction_keys):
        return 'some element in transation are missing',400
    index=blockchain.add_transaction(json['sender'],json['reciever'],json['amount'])
    response = {'message': f'This transaction will be added to Block {index}' }  # f
    return jsonify(response), 201

#########################connect new node to the chain
@app.route('/connect_node', methods=['POST'])
def connect_node():
    json=request.get_json()
    nodes=json.get('nodes')
    if nodes is None:
        return 'no node',400
    for node in nodes:
        blockchain.add_node(node)
    response={'message':'all node now connected','nodes:':list(blockchain.nodes)}
    return jsonify(response),201


#if __name__ == '__main__':
app.run(host='127.0.0.1', port=5000)
